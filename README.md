# dat2ld: Tool for Generating Linked Data from Union Catalog of Digital Archives Taiwan

author: [Cheng-Jen Lee](http://about.me/Sollee)

# Introduction

This tool is designed to generate linked data from the xml files exported from [Union Catalog of Digital Archives Taiwan](http://digitalarchives.tw/).

This tool consists of three main functions (steps): `xml2csv`, `csv2rdfcsv`, and `rdfcsv2rdf`.

* `xml2csv.py`
  * Step.1: Convert xml files of Union Catalog of Digital Archives Taiwan to csv files (formerly `xml2csv-v4.py`)

* `csv2rdfcsv_agent-project.py`, `csv2rdfcsv_d.py`, `csv2rdfcsv_r.py`
  * Step.2: Convert agent/project, D-ver, and R-ver csv files (from step.1) to rdf-like csv files.

* `rdfcsv2rdf.py`
  * Step.3: Convert rdf-like csv files (from step.2) to rdf files.

# Dependencies

* Python 3.4

* Python dependencies (Please install [pipenv](http://docs.pipenv.org/) first.)

```
pipenv install
```

* Only tested on Ubuntu (>= 14.04)

```
sudo apt-get install build-essential python3-dev
```

# Usage

* You can run the following scripts from the virtualenv created by pipenv. Just begin by

```
pipenv run python
```

* For example: (For simplicity, we will omit the `pipenv run` prefix in the remaining examples.)

```
pipenv run python xml2csv.py...
```

* Create a directory named `mapping` and put all mapping files into the directory (Mapping files are not available at this time. They will be released separately).

* `python xml2csv.py -f INPUT_CSV_FOLDER -x INPUT_XML_FOLDER -o OUTPUT_FOLDER`
  * INPUT\_CSV\_FOLDER: The folder of old csv files, which are also converted from xml files. We use old csv files to get the paths of xml files.
  * INPUT\_XML\_FOLDER: The folder of union catalog xml files.
  * OUTPUT\_FOLDER: The folder for output csv files.

* `python csv2rdfcsv_d.py -p PROFILE -f CSV_FILE -c CATEGORY -o OUTPUT_RDFCSV_FILE`
  * PROFILE: The JSON profile (in `profile/`).
  * CSV\_FILE: The csv file converted from xml files.
  * CATEGORY: The category (domain) of data.
  * OUTPUT\_RDFCSV\_FILE: The name for the output RDF-like csv file.

* `python csv2rdfcsv_agent-project.py -p PROFILE -f CSV_FILE -c CATEGORY -r OUTPUT_RDFCSV_FILE -o OUTPUT_MAPPING_FILE`
  * CSV\_FILE: The given csv file.
  * OUTPUT\_MAPPING\_FILE: The name for the output mapping csv file (used by CKAN display).
  * Other options are the same as `csv2rdfcsv_d.py`

* `python csv2rdfcsv_r.py -p PROFILE [-g GEO_MAPPING_FILE] [-t TIME_MAPPING_FILE] -f CSV_FILE -c CATEGORY -o OUTPUT_RDFCSV_FILE`
  * GEO\_MAPPING\_FILE: The geo-mapping file.
  * TIME\_MAPPING\_FILE: The time-mapping file.
  * Other options are the same as `csv2rdfcsv_d.py`
  * **Note that the first line of "prefix" in the profile MUST BE the version (r1, r2...) of R-ver csv.**

* `python rdfcsv2rdf.py -p PROFILE -f RDFCSV_FILE [-l OID_LIST] [-c CATEGORY] [-n FIRST_N_RECORDS] [-b BATCH_SIZE] -o OUTPUT_FOLDER`
  * PROFILE: The JSON profile (in `profile/`).
  * RDFCSV\_FILE: The RDF-like csv file.
  * OID\_LIST: The list containing OIDs (one line per OID) to be processed.
  * CATEGORY: The category (domain) of data.
  * FIRST\_N\_RECORDS: Convert first N records only.
  * BATCH\_SIZE: Max number of records for each rdf file [default: 30000].
  * OUTPUT\_FOLDER: The folder for output rdf files.

# Notices

* `xml2csv.py` is for reference purposes only since the old csv files may not be released.
* The default value of option "is_uri" in profile:
  * predicates under predicates: `false`
  * predicates under fixed_predicates: `true`
