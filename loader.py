"""Loader functions

Consists of functions to load mapping files.
"""
import os

import utils


def load_image_mapping():
    """D-ver

    Open the original and cached images mapping file.
    """
    image_mapping = dict()
    image_urls_r = utils.load_csv(
        "mapping/CC_Image_List.csv", fieldnames=["ID", "ori_url", "cached_url"], prompt=False)
    for url in image_urls_r:
        if not image_mapping.get(url["ID"]):
            image_mapping[url["ID"]] = []
        image_mapping[url["ID"]].append(url["cached_url"].strip())

    return image_mapping


def load_license_mapping():
    """Open the license mapping file.
    """
    license_mapping = dict()
    license_url_r = utils.load_csv("mapping/licenses.csv", prompt=False)
    for url in license_url_r:
        license_mapping[url["license"]] = url["uri"].strip()

    return license_mapping


def load_project_mapping():
    """Open the project mapping file.
    """
    project_mapping = dict()
    project_name_r = utils.load_csv(
        "mapping/Match_Agent_Project.csv", encoding="utf-8-sig", prompt=False)
    for prefixed_name in project_name_r:
        project_mapping[prefixed_name["record"]] = prefixed_name["uri"]

    return project_mapping


def load_geo_mapping(path=None):
    """R-ver

    Open the geo-mapping file.
    """
    geo_mapping = dict()
    if path:
        geo_r = utils.load_csv(path, encoding="utf-8-sig", prompt=False)
        for geo_info in geo_r:
            geo_mapping = utils.add_element_to_list_in_dict(
                geo_mapping, geo_info["OID"], geo_info)

    return geo_mapping


def load_time_mapping(path=None):
    """Open the time-mapping file.
    """
    time_mapping = dict()
    if path:
        time_r = utils.load_csv(path, encoding="utf-8-sig", prompt=False)
        time_mapping = {time_info["Date"]: time_info["normalized"]
                        for time_info in time_r if time_info["normalized"]}

    return time_mapping


def load_eol_mapping():
    """Open the EOL mapping file.
    """
    eol_mapping = dict()
    eol_filenames = os.listdir(os.path.normpath("mapping/eol"))
    for eol_filename in eol_filenames:
        eol_r = utils.load_csv(os.path.join(
            "mapping", "eol", eol_filename), encoding="utf-8-sig", prompt=False)
        for eol_info in eol_r:
            if eol_info["eol_id"]:
                eol_mapping[eol_info["OID"]] = {"eol_id": eol_info[
                    "eol_id"], "eol_title": eol_info["eol_title"]}

    return eol_mapping


def load_manual_time_mapping():
    """Open the manual-time-mapping file.
    """
    manual_time_mapping = dict()
    manual_time_r = utils.load_csv(
        "mapping/time/Time-notNormalized-mapping.csv", prompt=False)
    for time_info in manual_time_r:
        time_info["dct:date"] = ";".join(list(filter(
            None, [time_info["dct:date_1"], time_info["dct:date_2"], time_info["dct:date_3"]])))
        manual_time_mapping[time_info["Origional Data Value"]] = time_info

    return manual_time_mapping


def load_dct_label_mapping():
    """Open the rdfs:label mapping file for dct:temporal.
    """
    dct_label_r = utils.load_csv(
        "mapping/time/Time-notNormalized-mapping-rdfs_label.csv", prompt=False)
    dct_label_mapping = {x["wde:qID"]: x for x in dct_label_r}

    return dct_label_mapping


def load_r_mapping(domain):
    """Open the R-ver mapping table.
    """
    r_mapping = {}
    r_r = utils.load_csv("mapping/refined/R-mappingtables.csv", prompt=False)
    for r in r_r:
        if r["domain"] == domain:
            if r["Date::Field"]:
                r_mapping[r["Date::Field"]] = r
            if r["Coverage::Field"]:
                r_mapping[r["Coverage::Field"]] = r

    return r_mapping


def load_r_area_mapping():
    """Open the R-ver area mapping table.
    """
    area_r = utils.load_csv(
        "mapping/refined/R-mappingtables-area.csv", prompt=False)
    area_mapping = {area_info["DataValue"]: area_info[
        "DataValue-normalized"] for area_info in area_r}

    return area_mapping
