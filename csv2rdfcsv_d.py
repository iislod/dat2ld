"""csv2rdfcsv_d

Convert D-ver csv files to rdf-like csv files.
"""
import json
import time as pytime
from collections import OrderedDict
from argparse import RawTextHelpFormatter, ArgumentParser

from rdflib import Namespace, URIRef

import utils
import loader


parser = ArgumentParser(formatter_class=RawTextHelpFormatter)
parser.add_argument("-p", metavar="PROFILE",
                    help="the JSON profile", required=True)
parser.add_argument("-f", metavar="CSV_FILE",
                    help="the csv file converted from xml files", required=True)
parser.add_argument("-c", metavar="CATEGORY",
                    help="the category (domain) of data", required=True)
parser.add_argument("-o", metavar="OUTPUT_RDFCSV_FILE",
                    help="the name for the output RDF-like csv file", required=True)
args = parser.parse_args()
PROV_DATE_TAG = utils.get_prov_date_tag(suffix="d")

start_time = pytime.time()

mapping = json.load(open(args.p), object_pairs_hook=OrderedDict)
csv_reader = utils.load_csv(args.f, encoding="utf-8-sig")

image_mapping = loader.load_image_mapping()
license_mapping = loader.load_license_mapping()
project_mapping = loader.load_project_mapping()

# Construct the linked data tools namespace.
for prefix, url in mapping["prefix"].items():
    locals()[prefix] = Namespace(url)

# Add data to the graph.
oid = ""
out_row = {}
out_rows = []

for row in csv_reader:
    # If finding next OID, save previous graph.
    if row["OID"] and row["OID"] != oid:
        if oid != "":
            # r4r:hasLicense is same as cc:license_MetaDesc.
            out_row["r4r:hasLicense"] = out_row["cc:license_MetaDesc"]
            # Resolve the issue that items in "Record" may be duplicated.
            if out_row.get("prov:wasGeneratedBy"):
                out_row["prov:wasGeneratedBy"] = list(
                    set(out_row["prov:wasGeneratedBy"]))
            out_row_string = {k: "||".join(v) for k, v in out_row.items()}
            out_rows.append(out_row_string)

        oid = row["OID"]
        t_meta = data["d" + oid]
        t_prov = data[PROV_DATE_TAG + oid]
        out_row = {
            "subject_meta": [t_meta],
            "subject_prov": [t_prov],
            "r4r:locateAt": [t_meta],
            "dcat:themeTaxonomy": [data[args.c]],
            "r4r:hasProvenance": [t_prov],
            "data:objectID": [oid],
            "r4r:isPackagedWith": [t_meta]
        }
        if image_mapping.get(oid):
            for image in image_mapping[oid]:
                out_row = utils.add_element_to_list_in_dict(
                    out_row, "schema:thumbnail", URIRef(image))

    for k, v in row.items():
        if k == "Record":
            if project_mapping.get(v):
                out_row = utils.add_element_to_list_in_dict(
                    out_row, "prov:wasGeneratedBy", URIRef(project_mapping[v]))
        if v and mapping["mapping"].get(k, ""):
            if row.get(k + "::field"):
                v = "%s: %s" % (row[k + "::field"], v)
            if k in ["MetaDesc::license", "ICON::license"]:
                v = v.replace(" ", "")
                if v == "CC:3.0:BY-ND":
                    v = "CC3.0:BY-ND"
                if v == "CC2.5BY-NC-ND":
                    v = "CC2.5:BY-NC-ND"
                license_url = license_mapping[v]
                out_row = utils.add_element_to_list_in_dict(
                    out_row, "cc:license_" + k.split("::")[0], license_url)
            out_row = utils.add_element_to_list_in_dict(
                out_row, mapping["mapping"][k], v)

# Save the last one.
out_row["r4r:hasLicense"] = out_row["cc:license_MetaDesc"]
if out_row.get("prov:wasGeneratedBy"):
    out_row["prov:wasGeneratedBy"] = list(set(out_row["prov:wasGeneratedBy"]))
out_row_string = {k: "||".join(v) for k, v in out_row.items()}
out_rows.append(out_row_string)

# Output
fieldnames = [v for k, v in mapping["mapping"].items()] + mapping["others"]
utils.save_csv(fieldnames, out_rows, args.o)

print(pytime.time() - start_time)
