"""rdfcsv2rdf

Convert rdf-like csv files to rdf files.
"""
import os
import json
import time as pytime
from itertools import cycle
from argparse import RawTextHelpFormatter, ArgumentParser

from rdflib.term import _is_valid_uri
from rdflib import Graph, Namespace, URIRef, Literal

import utils


parser = ArgumentParser(formatter_class=RawTextHelpFormatter)
parser.add_argument("-p", metavar="PROFILE",
                    help="the JSON profile", required=True)
parser.add_argument("-f", metavar="RDFCSV_FILE",
                    help="the RDF-like csv file", required=True)
parser.add_argument("-l", metavar="OID_LIST",
                    help="the list containing OIDs to be processed")
parser.add_argument("-c", metavar="CATEGORY", default="",
                    help="the category (domain) of data")
parser.add_argument("-n", metavar="FIRST_N_RECORDS", type=int,
                    default=max, help="convert first N records only")
parser.add_argument("-b", metavar="BATCH_SIZE", type=int,
                    default=30000, help="max number of records for each rdf file")
parser.add_argument("-o", metavar="OUTPUT_FOLDER",
                    help="the folder for output rdf files", required=True)
args = parser.parse_args()


def init_graph():
    """Init a new graph.
    """
    g = Graph()

    # Bind name spaces.
    for prefix in mapping_file["prefix"].keys():
        g.bind(prefix, eval(prefix))

    return g


def string_to_pred(s):
    """Convert prefixed names to rdflib objects.
    """
    if not s:
        return None
    if s[:4] == "http":
        return URIRef(s.strip())

    [ns, prop] = s.split(":")

    return eval("%s['%s']" % (ns, prop))


def build_graph(graph, mapping, row, subject=None, subjects=[]):
    """Build graphs.
    """
    new_subjects = row[mapping["name"]] if mapping.get("name") else row[
        mapping["subject"]]
    if not new_subjects:
        return graph
    new_subjects = [s.strip() for s in new_subjects.split("||")
                    if _is_valid_uri(s.strip())]

    if mapping.get("predicates"):
        for pred in mapping["predicates"]:
            # If there is a "child" subject in this subject, go through it.
            if pred.get("subject"):
                for new_subject in new_subjects:
                    build_graph(graph, pred, row, new_subject, new_subjects)
            else:
                if not row[pred["name"]]:
                    continue
                objs = row[pred["name"]].split("||")
                # If there is a datatype_field, split it.
                datatypes = [string_to_pred(x) for x in row[pred["datatype_field"]].split("||")] if pred.get(
                    "datatype") == "from_field" else [string_to_pred(pred.get("datatype", None))] * len(objs)
                langs = row[pred["lang_field"]].split("||") if pred.get(
                    "lang") == "from_field" else [pred.get("lang", None)] * len(objs)
                zip_list = zip(new_subjects, objs, datatypes, langs) if len(new_subjects) == len(
                    objs) else zip(cycle(new_subjects), objs, datatypes, langs)
                for s, obj, datatype, lang in zip_list:
                    if not obj:
                        continue
                    if not _is_valid_uri(obj.strip()) and pred.get("is_uri", False):
                        continue
                    # If there is more than one subject.
                    if pred.get("is_plural", False):
                        zip_list2 = zip(s.split(";"), obj.split(";")) if len(s) == len(
                            obj) else zip(cycle(s.split(";")), obj.split(";"))
                        for sub_s, sub_obj in zip_list2:
                            if not sub_s or not sub_obj:
                                continue
                            graph.add((URIRef(sub_s),
                                       string_to_pred(pred["predicate"]) if pred.get(
                                "predicate") else string_to_pred(pred["name"]),
                                URIRef(sub_obj) if pred.get("is_uri", False) else Literal(sub_obj, datatype=datatype, lang=lang)))
                    # If there is only one subject.
                    else:
                        graph.add((URIRef(s),
                                   string_to_pred(pred["predicate"]) if pred.get(
                                       "predicate") else string_to_pred(pred["name"]),
                                   URIRef(obj.strip()) if pred.get("is_uri", False) else Literal(obj, datatype=datatype, lang=lang)))
                    # Add default objects.
                    for default_obj in pred.get("default_objects", []):
                        graph.add((URIRef(s),
                                   string_to_pred(pred["predicate"]) if pred.get(
                                       "predicate") else string_to_pred(pred["name"]),
                                   string_to_pred(default_obj)))

    if mapping.get("fixed_predicates"):
        for pred in mapping["fixed_predicates"]:
            for obj in pred["objects"]:
                if mapping.get("is_plural", False):
                    for s in new_subjects:
                        for sub_s in s.split(";"):
                            if not sub_s:
                                continue
                            graph.add((URIRef(sub_s),
                                       string_to_pred(pred["predicate"]),
                                       string_to_pred(obj) if pred.get("is_uri", True) else Literal(obj, lang=pred.get("lang"))))
                else:
                    for s in new_subjects:
                        if not s:
                            continue
                        graph.add((URIRef(s),
                                   string_to_pred(pred["predicate"]),
                                   string_to_pred(obj) if pred.get("is_uri", True) else Literal(obj, lang=pred.get("lang"))))

    if mapping.get("predicates_uri"):
        for pred in mapping["predicates_uri"]:
            if not s or not row[pred]:
                continue
            for s in new_subjects:
                graph.add((URIRef(s), string_to_pred(pred), URIRef(row[pred])))

    if subject:
        zip_list3 = zip(subjects, new_subjects) if len(subjects) == len(
            new_subjects) else zip(cycle(subjects), new_subjects)
        for old_subject, new_subject in zip_list3:
            if not old_subject:
                continue
            if mapping.get("is_plural", False):
                for sub_s in new_subject.split(";"):
                    if not sub_s:
                        continue
                    graph.add((URIRef(old_subject), string_to_pred(
                        mapping["subject"]), URIRef(sub_s)))
            else:
                graph.add((URIRef(old_subject), string_to_pred(
                    mapping["subject"]), URIRef(new_subject)))

    return graph

start_time = pytime.time()

# Load the json profile and rdf-like csv.
mapping_file = json.load(open(args.p))
mapping = mapping_file["mapping"]

# Make the output directory.
os.makedirs(os.path.abspath(args.o), exist_ok=True)
csv_reader = utils.load_csv(args.f)

# Load the OID list (if exists).
oid_list = []
if args.l:
    oid_list = utils.load_list(args.l)
    oid_list = ["http://data.odw.tw/record/d" + oid for oid in oid_list]

# Construct the linked data tools namespace.
for prefix, url in mapping_file["prefix"].items():
    locals()[prefix] = Namespace(url)

# Set default values.
g = init_graph()
batch_id = 0
category = args.c
pos = 0

# Build graphs.
for row in csv_reader:
    if oid_list and row["r4r:isPackagedWith"] not in oid_list:
        continue
    pos += 1
    g.add((URIRef(row["subject_prov"]),
           prov.startedAtTime,
           Literal(utils.get_taipei_time(), datatype=xsd.DateTime)))
    for graph in mapping:
        if graph.get("subject"):
            g = build_graph(g, graph, row)
    g.add((URIRef(row["subject_prov"]),
           prov.endedAtTime,
           Literal(utils.get_taipei_time(), datatype=xsd.DateTime)))

    if row.get("dcat:themeTaxonomy"):
        category = row["dcat:themeTaxonomy"].split("/")[-1]

    # Save graphs for the first args.n records.
    if pos == args.n:
        print("Got the first %d." % args.n)
        break

    # Save graphs for every args.b records.
    if pos % args.b == 0:
        batch_id = int(pos / args.b)
        print("Saving batch %d..." % batch_id)
        if not category:
            print("No category specified! Program terminated.")
            exit(1)
        g.serialize(destination=os.path.join(args.o, category +
                                             "-" + str(batch_id) + ".ttl"), format="turtle")
        g = init_graph()

# Save the last graph.
print("Saving batch %d..." % int(batch_id + 1))
if not category:
    print("No category specified! Program terminated.")
    exit(1)
g.serialize(destination=os.path.join(args.o, category + "-" +
                                     str(batch_id + 1) + ".ttl"), format="turtle")

if args.l:
    print("Processed %d OIDs." % pos)

print(pytime.time() - start_time)
