"""xml2csv

Convert xml files of Union Catalog of Digital Archives Taiwan to csv files.
"""
import os
import time
from argparse import RawTextHelpFormatter, ArgumentParser

from lxml import etree

import utils


parser = ArgumentParser(formatter_class=RawTextHelpFormatter)
parser.add_argument("-f", metavar="INPUT_CSV_FOLDER",
                    help="the folder of old csv files", required=True)
parser.add_argument("-x", metavar="INPUT_XML_FOLDER",
                    help="the folder of union catalog xml files", required=True)
parser.add_argument("-o", metavar="OUTPUT_FOLDER",
                    help="the folder for output csv files", required=True)
args = parser.parse_args()

start_time = time.time()
filenames = os.listdir(args.f)

for filename in filenames:
    result = []
    fieldnames = set()

    print("Scaning file: {filename}".format(filename=filename))
    csv_reader = utils.load_csv(
        args.f + "/" + filename, encoding="utf-8-sig", prompt=False)
    for pos, row in enumerate(csv_reader, 1):
        tree = etree.parse(os.path.join(
            args.x, row.get("DAURI")[-16:-5] + ".xml")).getroot()
        fieldnames.update(utils.scan(set(), tree))
    fieldnames = utils.classify_fieldnames(fieldnames)
    out_fieldnames = ["OID", "DAURI"] + \
        utils.reorder(fieldnames["a"], fieldnames["na"], fieldnames["a_tag"])

    print("Converting file: {filename}".format(filename=filename))
    csv_reader = utils.load_csv(
        args.f + "/" + filename, encoding="utf-8-sig", prompt=False)
    for pos, row in enumerate(csv_reader, 1):
        tree = etree.parse(os.path.join(
            args.x, row.get("DAURI")[-16:-5] + ".xml")).getroot()

        # Replace the newlines.
        xml_in_str = etree.tostring(tree)
        new_xml_data = xml_in_str.replace(b"\n", b"&#xD;")
        tree = etree.fromstring(new_xml_data)

        row_o = utils.dig(row.get("OID"), row.get("DAURI"),
                          out_fieldnames, fieldnames["na"], tree)
        result.extend(row_o)

    # Output
    print("Saving file: {filename}\n".format(filename=filename))
    utils.save_csv(out_fieldnames, result, args.o +
                   "/" + filename, "utf-8-sig")

print(pytime.time() - start_time)
