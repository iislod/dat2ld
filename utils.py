"""Utility functions

Consists of functions shared by this program.
"""
import os
import re
import csv
from urllib import parse
from datetime import datetime

from pytz import timezone
from rdflib import Namespace


GNS = Namespace("http://sws.geonames.org/")
REG = re.compile(r"\w*::\w*")
ADM_DESC = ["OID", "DAURI", "Project", "Record", "ICON",
            "Multimedia", "PDF", "Hyperlink", "DigiArchiveID"]
META_DESC = ["MetaDesc", "Title", "Creator", "Subject", "Description", "Publisher", "Contributor",
             "Date", "Type", "Format", "Identifier", "Source", "Language", "Relation", "Coverage", "Rights"]


def load_csv(f, encoding="utf-8", fieldnames=None, prompt=True):
    """Load csv and get the DictReader object.
    """
    if prompt:
        print("Processing %s..." % f)
    csv_file = open(os.path.normpath(f), encoding=encoding)
    csv_reader = csv.DictReader(csv_file, fieldnames=fieldnames)

    return csv_reader


def load_list(f):
    """Load a list containing OIDs and return unique OIDs.

    l: unique OIDs

    """
    list_file = open(os.path.normpath(f))
    l = list_file.read().splitlines()
    l = list(set(l))
    print("Loaded %d OID(s)." % len(l))

    return l


def save_csv(fieldnames, rows, f, encoding="utf-8"):
    """Save the DictWriter object to a csv file.
    """
    os.makedirs(os.path.dirname(os.path.abspath(f)), exist_ok=True)
    with open(os.path.normpath(f), "x", newline="", encoding=encoding) as fou:
        dw = csv.DictWriter(fou, fieldnames=fieldnames, dialect="unix")
        dw.writeheader()
        dw.writerows(rows)


def classify_fieldnames(fieldnames):
    """Split fieldnames into three categories.

    a: fieldnames with the pattern "a::b"
    a_tag: the "a" part in the pattern "a::b"
    na: fieldnames without the pattern "a::b"

    """
    sorted_fieldnames = sorted(fieldnames)

    out_a = [x for x in sorted_fieldnames if REG.match(x)]
    out_a_tag = [x.split("::")[0] for x in out_a]
    out_na = [x for x in sorted_fieldnames if (
        x not in out_a_tag and not REG.match(x))]

    return {"a": out_a,
            "a_tag": out_a_tag,
            "na": out_na}


def scan(out, root):
    """Scan xml files to get all fieldnames for a domain or project.
    """
    for child in root:
        if child.attrib:
            for attr, value in child.attrib.iteritems():
                element = "{tag}::{attr}".format(tag=child.tag, attr=attr)
                out.add(element)
            element = "{tag}".format(tag=child.tag)
            out.add(element)
        # If there exist child nodes, visit them.
        if len(child):
            out.update(scan(out, child))
        else:
            element = "{tag}".format(tag=child.tag)
            out.add(element)

    return out


def dig(oid, dauri, fieldnames, fieldnames_na, root):
    """Build csv records from xml files.
    """
    out = []

    for fieldname in fieldnames:
        if REG.match(fieldname):
            (tag, attr) = fieldname.split("::")
            update_flag = False
            if len(out):
                if tag in out[-1].keys():
                    update_flag = True
            childs = root.findall(".//" + tag)
            if update_flag:
                for i in range(len(childs)):
                    out[-len(childs) +
                        i].update({fieldname: childs[i].get(attr)})
            else:
                for child in childs:
                    if tag == "MetaDesc":
                        row = {"OID": oid, fieldname: child.get(attr)}
                    else:
                        row = {"OID": oid, fieldname: child.get(
                            attr), tag: child.text}
                    out.append(row)
        elif fieldname in fieldnames_na:
            for child in root.findall(".//" + fieldname):
                row = {"OID": oid, fieldname: child.text}
                out.append(row)

    out[0].update({"DAURI": dauri})

    return out


def reorder(fieldnames_a, fieldnames_na, fieldnames_a_tag):
    """Reorder the columns as desired (ADM_DESC and META_DESC).
    """
    out = []

    for adm in ADM_DESC:
        if adm in fieldnames_a_tag:
            matches = [x for x in fieldnames_a if adm == x.split("::")[0]]
            out.extend(matches)
            out.append(adm)
        if adm in fieldnames_na:
            out.append(adm)

    for meta in META_DESC:
        if meta in fieldnames_a_tag:
            matches = [x for x in fieldnames_a if meta == x.split("::")[0]]
            out.extend(matches)
            out.append(meta)
        if meta in fieldnames_na:
            out.append(meta)

    out.extend([x for x in fieldnames_a if x not in out])
    out.extend([x for x in fieldnames_na if x not in out])
    out.remove("MetaDesc")

    return out


def is_url(*args, **kw):
    """Returns True if argument parses as a http, https or ftp URL.
    """
    if not args:
        return False
    try:
        url = parse.urlparse(args[0])
    except ValueError:
        return False

    valid_schemes = ("http", "https", "ftp")

    return url.scheme in valid_schemes


def get_prov_date_tag(suffix=""):
    """Generate a prefix to indicate the date of the provenance information.
    """

    return "p%s-%s" % (datetime.strftime(datetime.today(), "%Y%m%d"), suffix)


def add_element_to_list_in_dict(d, key, l_value):
    """Add a element to a list in a dict.
    """
    # If the key does not exist, create a new list with that key.
    if key not in d:
        d[key] = []

    if isinstance(l_value, list):
        d[key].extend(l_value)
    else:
        d[key].append(l_value)

    return d


def process_geonames(geo_list):
    """Build the geonames-related columns.
    """
    geo_lat = ""
    geo_long = ""
    gns_iris = []
    gns_names_en = []
    gns_names_zh = []

    for geo in geo_list:
        gns_iri = list(filter(None, geo["gns:id"].split(";")))
        gns_iris.extend([GNS[x] for x in gns_iri])
        gns_names_en.extend(list(filter(None, geo["gns:nameEN"].split(";"))))
        gns_names_zh.extend(list(filter(None, geo["gns:nameZH"].split(";"))))
        if geo["Coverage::lat"]:
            geo_lat = geo["Coverage::lat"]
        if geo["Coverage::long"]:
            geo_long = geo["Coverage::long"]

    return {"lat": geo_lat,
            "long": geo_long,
            "iris": ";".join(gns_iris),
            "names_en": ";".join(gns_names_en),
            "names_zh": ";".join(gns_names_zh)}


def get_taipei_time(tz="Asia/Taipei"):
    """Get the Taipei time in the ISO8601 format.
    """

    return timezone(tz).localize(datetime.now()).isoformat()
