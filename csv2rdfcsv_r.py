"""csv2rdfcsv_r

Convert R-ver csv files to rdf-like csv files.
"""
import json
import time as pytime
from collections import OrderedDict
from argparse import RawTextHelpFormatter, ArgumentParser

from rdflib import Namespace, URIRef

import utils
import loader


parser = ArgumentParser(formatter_class=RawTextHelpFormatter)
parser.add_argument("-p", metavar="PROFILE",
                    help="the JSON profile", required=True)
parser.add_argument("-g", metavar="GEO_MAPPING_FILE",
                    help="the geo-mapping file")
parser.add_argument("-t", metavar="TIME_MAPPING_FILE",
                    help="the time-mapping file")
parser.add_argument("-f", metavar="CSV_FILE",
                    help="the csv file converted from xml files", required=True)
parser.add_argument("-c", metavar="CATEGORY",
                    help="the category (domain) of data", required=True)
parser.add_argument("-o", metavar="OUTPUT_RDFCSV_FILE",
                    help="the name for the output RDF-like csv file", required=True)
args = parser.parse_args()
PROV_DATE_TAG = utils.get_prov_date_tag(suffix="d")


def cov_prefix(s):
    """Convert prefixed names to rdflib objects.
    """
    if not s:
        return

    iri = s.replace("^^", "")
    (prefix, prop) = [x.strip() for x in iri.split(":")]

    return eval(prefix)[prop]


start_time = pytime.time()

mapping = json.load(open(args.p), object_pairs_hook=OrderedDict)
version = list(mapping["prefix"].items())[0][0]
csv_reader = utils.load_csv(args.f, encoding="utf-8-sig")

eol_mapping = loader.load_eol_mapping()
geo_mapping = loader.load_geo_mapping(args.g)
time_mapping = loader.load_time_mapping(args.t)
manual_time_mapping = loader.load_manual_time_mapping()
dct_label_mapping = loader.load_dct_label_mapping()
r_all_mapping = loader.load_r_mapping(args.c)
area_mapping = loader.load_r_area_mapping()

# Construct the linked data tools namespace.
for prefix, url in mapping["prefix"].items():
    locals()[prefix] = Namespace(url)

# Add data to the graph.
oid = ""
out_row = {}
out_rows = []

for row in csv_reader:
    # If finding next OID, save previous graph.
    if row["OID"] and row["OID"] != oid:
        if oid != "":
            # Add geonames for Coverage which is not an event out do not have a
            # Coverage::field.
            gns_nofield = utils.process_geonames(geo_info)
            out_row = utils.add_element_to_list_in_dict(
                out_row, "dct:spatial", gns_nofield["iris"])
            out_row = utils.add_element_to_list_in_dict(
                out_row, "rdfs:label_dct:spatial_en", gns_nofield["names_en"])
            out_row = utils.add_element_to_list_in_dict(
                out_row, "rdfs:label_dct:spatial_zh", gns_nofield["names_zh"])
            if not gns_nofield["iris"] and geo_info:
                for geo in geo_info:
                    out_row = utils.add_element_to_list_in_dict(
                        out_row, "schema:location_subject", geo["Coverage"])
            # Generate out row for current OID.
            out_row_string = {k: "||".join(v) for k, v in out_row.items()}
            out_rows.append(out_row_string)

        oid = row["OID"]
        t_meta = data["d" + oid]
        t_prov = data[PROV_DATE_TAG + oid]
        out_row = {
            "subject_meta": [t_meta],
            "subject_prov": [t_prov],
            "r4r:locateAt": [t_meta],
            "dcat:landingPage": [eval(version)["%s-r%s" % (version, oid)]],
            "dcat:themeTaxonomy": [data[args.c]],
            "r4r:hasProvenance": [t_prov],
            "prov:wasInfluencedBy": [eval(version)["%s-r%s" % (version, oid)]],
            "r4r:isPackagedWith": [t_meta]
        }
        if eol_mapping.get(oid):
            eol_info = eol_mapping[oid]
            out_row = utils.add_element_to_list_in_dict(
                out_row, "txn:hasEOLPage", eol[eol_info["eol_id"]])
            out_row = utils.add_element_to_list_in_dict(
                out_row, "rdfs:label_txn:hasEOLPage", eol_info["eol_title"])

        geo_info = geo_mapping.get(oid, [])

    for k, v in row.items():
        if k in ["Coverage", "Date"]:
            field = row.get(k + "::field")

            geos = []
            for g in geo_info:
                if field and g["Coverage::field"] == field:
                    geos.append(g)
                    geo_info.remove(g)

            gns_field = utils.process_geonames(geos)

            time = time_mapping.get(v, "")

            manual_time = manual_time_mapping.get(v, {})

            if r_all_mapping.get(field):
                r_all = r_all_mapping[field]

                # Basic properties
                for k2, v2 in mapping["mapping"].items():
                    if k2 == "event ID":
                        out_row = utils.add_element_to_list_in_dict(
                            out_row, v2, evt84[r_all[k2][6:-4] + "d" + oid])
                    elif k2 == "skos:scopeNote":
                        out_row = utils.add_element_to_list_in_dict(
                            out_row, v2, r_all[k2][1:-5])
                    elif r_all[k2] and k2 in ["rdf:type", "skos:inScheme", "event:factor", "event:product"]:
                        out_row = utils.add_element_to_list_in_dict(
                            out_row, v2, ";".join([cov_prefix(x) for x in r_all[k2].split(";")]))
                    else:
                        out_row = utils.add_element_to_list_in_dict(
                            out_row, v2, r_all[k2])

                # Time-related properties
                for in_field, out_field in mapping["time"].items():
                    r_date_value = r_all.get(in_field, "")
                    t_value = ""
                    if r_date_value:
                        t_value = time
                    if in_field == "dct:date_datatype" and r_all["dct:date"] and time:
                        t_value = cov_prefix(r_all["dct:date"])
                    out_row = utils.add_element_to_list_in_dict(
                        out_row, out_field, t_value)

                # Manual-time-related properties
                for in_field, out_field in mapping["time_manual"].items():
                    t_manual_value = manual_time.get(in_field, "")
                    if t_manual_value and in_field in ["event:time", "time:intervalStarts", "time:intervalFinishes", "time:intervalBefore", "time:intervalAfter", "voc:lastIntervalOf", "voc:initialIntervalOf"]:
                        t_manual_value = ";".join(
                            [cov_prefix(x) for x in t_manual_value.split(";")])

                        rdfs_label_en = ";".join([dct_label_mapping.get(x, {}).get(
                            "rdfs:label (@en)", "") for x in manual_time.get(in_field, "").split(";") if x])
                        rdfs_label_zh = ";".join([dct_label_mapping.get(x, {}).get(
                            "rdfs:label (@zh)", "") for x in manual_time.get(in_field, "").split(";") if x])
                        out_row = utils.add_element_to_list_in_dict(
                            out_row, "rdfs:label_" + in_field + "_en", rdfs_label_en)
                        out_row = utils.add_element_to_list_in_dict(
                            out_row, "rdfs:label_" + in_field + "_zh", rdfs_label_zh)
                    if in_field in ["dct:date", "schema:startDate", "schema:endDate"]:
                        if t_manual_value:
                            out_row[out_field] = [t_manual_value]
                            if r_all["dct:date"]:
                                out_row["dct:date_datatype"] = [
                                    cov_prefix(r_all["dct:date"])]
                        continue
                    out_row = utils.add_element_to_list_in_dict(
                        out_row, out_field, t_manual_value)

                # Spatial-related properties ("gns:ID" in the R-mapping)
                for in_field, out_field in mapping["spatial"].items():
                    out_row = utils.add_element_to_list_in_dict(out_row, out_field, gns_field[
                                                                "iris"] if r_all.get(in_field) == "gns:ID" else "")
                    out_row = utils.add_element_to_list_in_dict(
                        out_row, "rdfs:label_" + in_field + "_en", gns_field["names_en"])
                    out_row = utils.add_element_to_list_in_dict(
                        out_row, "rdfs:label_" + in_field + "_zh", gns_field["names_zh"])
                out_row = utils.add_element_to_list_in_dict(
                    out_row, "geo:lat", gns_field["lat"])
                out_row = utils.add_element_to_list_in_dict(
                    out_row, "geo:long", gns_field["long"])

                # General properties
                for in_field, out_field in mapping["general"].items():
                    out_field_value = ""
                    if r_all.get(in_field):
                        if in_field == "schema:polygon":
                            out_field_value = area_mapping[row[k]]
                        elif in_field == "dwc:locality":
                            if args.c == "Biology" and not gns_field["iris"]:
                                out_field_value = row[k]
                        else:
                            out_field_value = row[k]
                    if in_field == "dwc:locality_datatype":
                        if args.c == "Biology" and not gns_field["iris"] and r_all["dwc:locality"][:2] == "^^":
                            out_field_value = cov_prefix(r_all["dwc:locality"])
                    if in_field == "dwc:locality_lang":
                        if args.c == "Biology" and not gns_field["iris"] and "@" in r_all["dwc:locality"]:
                            out_field_value = r_all[
                                "dwc:locality"].split("@", 1)[-1]
                    out_row = utils.add_element_to_list_in_dict(
                        out_row, out_field, out_field_value)

            else:
                if manual_time:
                    out_row = utils.add_element_to_list_in_dict(
                        out_row, "dct:date_subject", manual_time["dct:date"])
                    dct_temporals = [cov_prefix(x) for x in manual_time[
                        "dct:temporal"].split(";") if x]
                    out_row = utils.add_element_to_list_in_dict(
                        out_row, "dct:temporal", ";".join(dct_temporals))
                    rdfs_label_dct_temporals_en = [dct_label_mapping.get(x, {}).get(
                        "rdfs:label (@en)", "") for x in manual_time["dct:temporal"].split(";") if x]
                    out_row = utils.add_element_to_list_in_dict(
                        out_row, "rdfs:label_dct:temporal_en", rdfs_label_dct_temporals_en)
                    rdfs_label_dct_temporals_zh = [dct_label_mapping.get(x, {}).get(
                        "rdfs:label (@zh)", "") for x in manual_time["dct:temporal"].split(";") if x]
                    out_row = utils.add_element_to_list_in_dict(
                        out_row, "rdfs:label_dct:temporal_zh", rdfs_label_dct_temporals_zh)

# Save the lasa one.
out_row_string = {k: "||".join(v) for k, v in out_row.items()}
out_rows.append(out_row_string)

# Output
fieldnames = [v for k, v in mapping["mapping"].items()] + \
             [v for k, v in mapping["general"].items()] + \
             [v for k, v in mapping["time"].items()] + \
             [v for k, v in mapping["time_manual"].items()] + \
             [v for k, v in mapping["spatial"].items()] + \
    mapping["others"]
fieldnames = list(OrderedDict.fromkeys(fieldnames).keys())
utils.save_csv(fieldnames, out_rows, args.o)

print(pytime.time() - start_time)
