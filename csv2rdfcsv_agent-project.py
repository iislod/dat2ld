"""csv2rdfcsv_agent-project

Convert agent/project csv files to rdf-like csv files.
"""
import json
from collections import OrderedDict
from argparse import RawTextHelpFormatter, ArgumentParser

from rdflib import Namespace, URIRef
from rdflib.namespace import SKOS, DCTERMS

import utils


parser = ArgumentParser(formatter_class=RawTextHelpFormatter)
parser.add_argument("-p", metavar="PROFILE",
                    help="the JSON profile", required=True)
parser.add_argument("-f", metavar="CSV_FILE",
                    help="the given csv file", required=True)
parser.add_argument("-c", metavar="CATEGORY",
                    help="the category (domain) of data", required=True)
parser.add_argument("-r", metavar="OUTPUT_RDFCSV_FILE",
                    help="the name for the output RDF-like csv file", required=True)
parser.add_argument("-o", metavar="OUTPUT_MAPPING_FILE",
                    help="the name for the output mapping csv file (used by CKAN display)", required=True)
args = parser.parse_args()
PROV_DATE_TAG = utils.get_prov_date_tag()

mapping = json.load(open(args.p), object_pairs_hook=OrderedDict)
csv_reader = utils.load_csv(args.f, encoding="utf-8-sig")

# Construct the linked data tools namespace.
for prefix, url in mapping["prefix"].items():
    locals()[prefix] = Namespace(url)
skos = SKOS
dct = DCTERMS

# Create dict output mapping CSV.
out_rows_rdf = []
out_rows_mapping = []

for row in csv_reader:
    id = row["skos:narrowMatch"].strip()[4:].lower()
    t_meta = eval(args.c)[id]
    t_prov = eval(args.c)[PROV_DATE_TAG + id]
    out_row_rdf = {
        "subject_meta": t_meta.toPython(),
        "subject_prov": t_prov.toPython(),
        "r4r:locateAt": t_meta.toPython(),
        "r4r:hasProvenance": t_prov.toPython(),
        "r4r:isPackagedWith": t_meta.toPython()
    }

    for k, v in row.items():
        if v and mapping["mapping"].get(k, ""):
            if k in ["rdf:type", "skos:narrowMatch", "dct:isPartOf", "org:memberOf", "org:role", "prov:wasAssociatedWith"]:
                v = eval(v.replace(":", ".")).toPython()
            if utils.is_url(v):
                v = v.strip()
            out_row_rdf[mapping["mapping"][k]] = v

    out_row_mapping = {
        "uri": t_meta.toPython(),
        args.c: out_row_rdf.get("rdfs:label_zh", ""),
        "abbreviation": row.get("abbreviation", "")
    }

    out_rows_rdf.append(out_row_rdf)
    out_rows_mapping.append(out_row_mapping)

# Output
fieldnames_rdf = [v for k, v in mapping["mapping"].items()] + mapping["others"]
utils.save_csv(fieldnames_rdf, out_rows_rdf, args.r)

fieldnames_mapping = ["uri", args.c, "abbreviation"]
utils.save_csv(fieldnames_mapping, out_rows_mapping, args.o)
